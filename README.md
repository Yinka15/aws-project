

## commercial-tf-modules

Collection of Terraform modules for use by the Sev1Tech Commercial Business Unit.

## Quick Links

* [EC2](aws/ec2/README.md)
* [ELB](aws/elb/README.md)
* [IAM](aws/iam/README.md)
* [S3](aws/s3/README.md)
* [S3 Backend](aws/s3-backend/README.md)

## Deployment Commands

After `config/backend-${env}.conf` and `config/${env}.tfvars` are configured you can run the Terraform code to `init`, `plan`, and `apply`:

1. Set the environment variable (example environment is dev):
```bash
env=dev
```

2. Download and install modules needed for the configuration given the path.
```bash
terraform get -update=true
```

3. Initialize a new or existing Terraform working directory by creating initial files, loading any remote state, downloading modules, etc.
```bash
terraform init -backend-config=config/backend-${env}.conf
```

4. Generate a speculative execution plan, showing what actions Terraform would take to apply the current configuration. This command will not actually perform the planned actions.

```bash
terraform plan -var-file=config/${env}.tfvars
```

5. Create or update infrastructure according to Terraform configuration files in the current directory.
```bash
terraform apply -var-file=config/${env}.tfvars
```

6. **(Optional)** Terraform Destroy
```bash
terraform destroy -var-file=config/${env}.tfvars
```

**NOTE: Troubleshooting**
If you run into any issues with the remote backend, try deleting the cache folder and running the commands again.
```bash
rm -rf .terraform
```

## Feature Development

Features Development correspond to the Sev1Tech Jira Board **TFCOMM** [link](https://sev1tech.atlassian.net/jira/software/projects/TFCOMM/boards/18)

When working on a specific ticket example: `TFCOMM-<ticket number>` or specifically `TFCOMM-1` for example, create a new branch off of the `main` branch with the naming convention `feature/TFCOMM-<ticket number>` or specifically `feature/TFCOMM-1` for example.

```bash
$ git checkout -b feature/TFCOMM-1
Switched to a new branch 'feature/TFCOMM-1'
```

On the new feature branch create make any modification to the code and repository as needed in the referencing ticket and when you are ready to commit your changes, push the remote branch.

**Example of Pushing Remote Branch**

1. Check local Git Status:
```bash
git status
On branch feature/TFCOMM-1
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   .gitignore
	modified:   README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	modules/
	s3-backend/README.md

no changes added to commit (use "git add" and/or "git commit -a")
```

2. Add any files that you that you want to commit to the pull request. You can perform this step multiple times or include a directory.
```bash
$ git add .gitignore
```

3. Include a commit message.
```bash
$ git commit -m "update .gitignore"
[feature/TFCOMM-1 b920dbd] update .gitignore
 1 file changed, 3 insertions(+)
```

4. Push the remote repository
```bash
$ git push origin feature/TFCOMM-1
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 12 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 408 bytes | 408.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote: Resolving deltas: 100% (1/1), completed with 1 local object.
remote: 
remote: Create a pull request for 'feature/TFCOMM-1' on GitHub by visiting:
remote:      https://github.com/Sev1Tech/commercial-tf-modules/pull/new/feature/TFCOMM-1
remote: 
To github.com:Sev1Tech/commercial-tf-modules.git
 * [new branch]      feature/TFCOMM-1 -> feature/TFCOMM-1
```

**Create the Pull Request**

Once you've pushed the remote branch, you can then create a pull request in GitHub with comments and notify a code reviewer.
![Pull Request Example](docs/img/pr.png)