variable "load_balancer_arn" {
  default     = ""
  description = "arn of loadbalancer"
}
variable "port" {
  default     = ""
  description = "listner port of loadbalancer"
}
variable "protocol" {
  default     = ""
  description = "listner protocol of loadbalancer"
}
variable "certificate_arn" {
  default     = ""
  description = "arn need to add in loadbalancer listner"
}
variable "default_action" {
  default     = ""
  description = "Action that loadbalacner should perform"
}
