resource "aws_lb_listener" "front_end" {
  load_balancer_arn = var.load_balancer_arn
  port              = var.port
  protocol          = var.protocol
  certificate_arn   = var.certificate_arn

  default_action {
    type = var.default_action.type

    fixed_response {
      content_type = var.default_action.fixed_response.content_type
      message_body = var.default_action.fixed_response.message_body
      status_code  = var.default_action.fixed_response.status_code
    }
  }
}
