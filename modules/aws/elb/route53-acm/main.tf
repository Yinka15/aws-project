resource "aws_acm_certificate" "acm_cert" {
  domain_name       = aws_route53_record.route53.fqdn
  validation_method = var.validation_method
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    Name = var.tags
  }
}

resource "aws_route53_record" "cert_validation" {
  allow_overwrite = true
  name            = tolist(aws_acm_certificate.acm_cert.domain_validation_options)[0].resource_record_name
  records         = [tolist(aws_acm_certificate.acm_cert.domain_validation_options)[0].resource_record_value]
  type            = tolist(aws_acm_certificate.acm_cert.domain_validation_options)[0].resource_record_type
  zone_id         = var.zone_id
  ttl             = 60
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.acm_cert.arn
  validation_record_fqdns = [aws_route53_record.cert_validation.fqdn]
}

resource "aws_route53_record" "route53" {
  zone_id = var.zone_id
  name    = var.name
  type    = var.type
  alias {
    name                   = var.alias.name
    zone_id                = var.alias.zone_id
    evaluate_target_health = var.alias.evaluate_target_health
  }
}
