
variable "name" {
  default     = ""
  description = "Nmae of the domain"
}

variable "zone_id" {
  default     = ""
  description = "Route 53 public id"
}

variable "type" {
  default     = ""
  description = "Type of Route53 domain"
}

variable "alias" {
  default     = ""
  description = "Configuration of alias like zoneid and alb"
}

variable "validation_method" {
  default     = ""
  description = "Validation method DNS or EMAIL"
}

variable "tags" {
  default     = ""
  description = "tags for the ACM certificate"
}
