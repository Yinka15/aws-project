
variable "terraform_instance" {
    type = list(string)
    default = ["PRD","DEV","STG"]
    }


variable "instance_type" {
  description = "instance type for ec2"
  default     =  "t2.micro"
}

variable "instance_count" {
  default = "3"
}

variable "ami_name" {
  default = "ami-00e87074e52e6c9f9"
}

variable "manager_count" {
    default = "3"

}


# variable "public_key" {
#     default = "you public key start with =ssh-rsa "
# }

variable "key_name" {
    default = "Sev1"
}


variable "key_pair_names" {
    description = "EC2 Key pair names"
    default = "3"
}

