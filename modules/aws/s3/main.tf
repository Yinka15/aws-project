
#This template can create multiple s3_buckets but make sure to provide a unique name for each bucket.

resource "aws_s3_bucket" "Sev1_bucket" {      
   count = "${length(var.s3_bucket_name)}"      
   bucket = "${var.s3_bucket_name[count.index]}"
   acl = "private"
   versioning {
      enabled = true
   }
   force_destroy = "true"
}
